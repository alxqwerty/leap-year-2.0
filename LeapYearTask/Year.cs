﻿using System;

namespace LeapYearTask
{
    public static class Year
    {
        public static bool IsLeapYear(int year)
        {
            bool isLeapYear = false;
            int baseLeap = 2020;

            if ((baseLeap - year) % 4 == 0)
            {
                isLeapYear = true;
            }

            if (year % 100 == 0 && year % 400 != 0)
            {
                isLeapYear = false;
            }

            return isLeapYear;
        }
    }
}